// DB connection
const mysql = require('mysql')

var con = mysql.createConnection({
    host: 'mysqld',
    user: 'root',
    password: '123456',
    database: 'app_full',
    multipleStatements: true
  })
  
  con.connect((err) => {
    if (!err) 
      console.log('Connection Established Successfully');
    else
      console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
  })

module.exports = con