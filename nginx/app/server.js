const express = require('express');
const randon_name = require('node-random-name')
const con = require('./database')

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';



const query_create = "CREATE TABLE IF NOT EXISTS usuarios ( id INT UNSIGNED NOT NULL AUTO_INCREMENT,  name VARCHAR(50),  PRIMARY KEY (id))"

con.query(query_create, (err, rows) => {
  if (err) throw err
})


// App
const app = express();
app.set('view engine', 'pug')

app.get('/', function(req, res) {
  con.query(`INSERT  into usuarios(name) values ('${randon_name()}')`, (err, rows) => {
    if (err) throw err
  })

  con.query('SELECT * FROM usuarios', (err, rows, filds) => {
    if (!err){
      var users = []
      rows.forEach(function(row) {
        users.push(row.name)
      })
      res.render('index', {title:'Desafio Nginx', message: 'Full Cycle Rocks!',  users: users});
    }else{
      console.log(err)
    }
      
  })
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});